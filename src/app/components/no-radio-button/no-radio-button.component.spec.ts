import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoRadioButtonComponent } from './no-radio-button.component';

describe('NoRadioButtonComponent', () => {
  let component: NoRadioButtonComponent;
  let fixture: ComponentFixture<NoRadioButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoRadioButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
